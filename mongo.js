// CREATE

// db.users.insertOne({
//     "username": "jellyace",
//     "password": "jelly987"
// })

// db.users.insertOne({
//     "username": "cupcake",
//     "password": "cupcale456"
// })

// insert multiple documents at once

// db.users.insertMany(
//     [
//         {
//             "username": "lollipop",
//             "password": "lolli863"
//         },
//         {
//             "username": "candy",
//             "password": "candy752"
//         }
//     ]
// )

// db.products.insertMany([
//     {
//         "name": "Dell Notebook",
//         "description": "Inspiron 15",
//         "price": 45000
//     },
//     {
//         "name": "Acer Notebook",
//         "description": "Inspire 15",
//         "price": 35000
//     },
//     {
//         "name": "HP Notebook",
//         "description": "Pavillion 15",
//         "price": 65000
//     }
// ])

// READ / RETRIEVE

// db.users.find() // return/fnd all docs in the collections
// db.users.find({"username": "candy"})

// db.cars.insertMany(
//     [
//         {
//             "name": "Vios",
//             "brand": "Toyota",
//             "type": "sedan",
//             "price": 1500000
//         },
//         {
//             "name": "Tamaraw FX",
//             "brand": "Toyota",
//             "type": "auv",
//             "price": 750000
//         },
//         {
//             "name": "City",
//             "brand": "Honda",
//             "type": "sedan",
//             "price": 1600000
//         }
//     ]
// )

// db.cars.find({"type": "sedan"})
// db.cars.find({"brand": "Toyota"})
// db.cars.findOne({}) - return the first item
// db.cars.findOne({"type": "sedan"}) - return the first type item
// db.cars.findOne({"brand": "Toyota"})
// db.cars.findOne({"brand": "Honda"})

// UPDATE

//db.users.updateOne({"username": "lollipop"}, {$set:{"username": "chocolate"}})

// update automatically the first item
//db.users.update({}, {$set:{"username": "updatedUsername"}})
// If field being updated doesn't exist, will add that filed in the document
//db.users.update({"username": "chocolate"}, {$set:{"isAdmin": true}})

// update all items in collection 
//db.users.updateMany({}, {$set:{"isAdmin": true}})

// {criteria: value}, $set{field: updatedValue}
// db.cars.updateMany({"type": "sedan"}, {$set:{"price": 1000000}})

// DELETE

// delete the first document
// db.products.deleteOne({})

// first item that match the criteria will be deleted
// db.cars.deleteOne({"brand": "Toyota"})

// delete ALL users where admin is true / matches the criteria
// db.users.deleteMany({"isAdmin": true})